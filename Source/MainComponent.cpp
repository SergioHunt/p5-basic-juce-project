/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //Buttons
    //Button1
    textButton1.setButtonText("Click me");
    //addAndMakeVisible(textButton1);
    textButton1.addListener(this);
    //Button2
    textButton2.setButtonText("Click me too");
    //addAndMakeVisible(textButton2);
    textButton2.addListener(this);
   
    //Sliders
    slider1.setSliderStyle(Slider::LinearHorizontal);
    //addAndMakeVisible(slider1);
    slider1.addListener(this);
    
    //ComboBox
    comboBox1.addItem("Item1", 1);
    comboBox1.addItem("Item2", 2);
    //addAndMakeVisible(comboBox1);
    
    //Text
    textEditor1.setText ("Add some text here");
    //addAndMakeVisible (textEditor1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG(" Get height :" << getHeight());
    DBG(" Get width :" << getWidth());
    //Buttons
    textButton1.setBounds(10, 10, getWidth() - 20, 40);
    textButton2.setBounds(10, 20, getWidth() - 20, 40);
    //Sliders
    slider1.setBounds (10, 60, getWidth() - 20, 40);
    
    //ComboBox
    comboBox1.setBounds (10, 100, getWidth()-20, 40);
    
    //Text
    textEditor1.setBounds (10, 140, getWidth()-20, 40);
    
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &textButton1)
        DBG ("textButton1 clicked!\n");
    else if (button == &textButton2)
        DBG ("textButton2 clicked!\n");
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG ("Slider value = \n" << slider1.getValue());
}

void MainComponent::paint (Graphics& g)
{
    int halfWidth = getWidth()/2;
    int halfHeight = getHeight()/2;
   g.setColour(Colours::hotpink);
   g.drawEllipse (0, 0, getWidth(), getHeight(), 1);
    
    DBG ("Paint Function is being Called");
}
