/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public Button::Listener, public Slider::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    

    void buttonClicked (Button* button) override;
    void sliderValueChanged (Slider* slider) override;
    void paint (Graphics& g) override;
    
    
private:

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    
    //Buttons
    TextButton textButton1;
    TextButton textButton2;
    
    //Sliders
    Slider slider1;
    
    //DropBoxs
    ComboBox comboBox1;
    
    //Text
    TextEditor textEditor1;
};


#endif  // MAINCOMPONENT_H_INCLUDED
